﻿using BinanceTest.Model.REST_Response;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BinanceTest.Converters
{
    /// <summary>
    /// Кастомный преобразователь некоторых типов объектов JSON
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class JsonCreationConverter<T> : JsonConverter
    {

        protected abstract T Create(Type objectType, JObject jObject);

        /// <summary>
        /// Условие, должен ли преобразовываться тип
        /// </summary>
        /// <param name="objectType"></param>
        /// <returns></returns>
        public override bool CanConvert(Type objectType)
        {
            //Если тип унаследован от интерфейса, который определяет возможность конвертации
            return objectType.GetInterfaces().Contains(typeof(IJsonConvertable));
        }

        public JsonCreationConverter()
        {

        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override object ReadJson(JsonReader reader,
                                        Type objectType,
                                         object existingValue,
                                         JsonSerializer serializer)
        {



            if (reader.TokenType == JsonToken.Null)
                return null;

            JObject jObject = JObject.Load(reader);

            //
            objectType = GetSymbolFilterType(jObject);           

            //создание объекта из JSON записи
            T target = Create(objectType, jObject);

            //заполнение значений созданного объекта
            using (JsonReader jObjectReader = CopyReaderForObject(reader, jObject))
            {
                serializer.Populate(jObjectReader, target);
            }

            return target;



        }

        /// <summary>
        /// Получение типа объекта Json в зависимости от наличия в нем ключа filterType. В зависимости
        /// от значения ключа, определяется тип заданного фильтра тикера
        /// </summary>
        /// <param name="jObject"></param>
        /// <returns></returns>
        private Type GetSymbolFilterType(JObject jObject)
        {
            var typeTokenVal = jObject.SelectToken("filterType").Value<string>();
            switch (typeTokenVal)
            {
                case "PRICE_FILTER":
                    return typeof(SymbolFilterPrice);
                case "LOT_SIZE":
                    return typeof(SymbolFilterLotSize);
                case "MARKET_LOT_SIZE":
                    return typeof(SymbolFilterMarketLotSize);
                case "MAX_NUM_ORDERS":
                    return typeof(SymbolFilterMaxNumOrders);
                case "MAX_NUM_ALGO_ORDERS":
                    return typeof(SymbolFilterMaxNumAlgoOrders);
                case "MIN_NOTIONAL":
                    return typeof(SymbolFilterMaxNotional);
                case "PERCENT_PRICE":
                    return typeof(SymbolFilterPercentPrice);
                default:
                    throw new Exception($"Неизвестный тип для {nameof(SymbolFilter)}.");
            }
        }

        /// <summary>
        /// Создание копии ридера JSON для объекта.
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="jToken"></param>
        /// <returns></returns>
        private JsonReader CopyReaderForObject(JsonReader reader, JToken jToken)
        {
            JsonReader jTokenReader = jToken.CreateReader();
            jTokenReader.Culture = reader.Culture;
            jTokenReader.DateFormatString = reader.DateFormatString;
            jTokenReader.DateParseHandling = reader.DateParseHandling;
            jTokenReader.DateTimeZoneHandling = reader.DateTimeZoneHandling;
            jTokenReader.FloatParseHandling = reader.FloatParseHandling;
            jTokenReader.MaxDepth = reader.MaxDepth;
            jTokenReader.SupportMultipleContent = reader.SupportMultipleContent;
            return jTokenReader;
        }


    }



    /// <summary>
    /// Кастомный преобразователь JSON
    /// </summary>
    public class ObjectJsonConverter : JsonCreationConverter<IJsonConvertable>
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }


        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {

            var jsonLineInfo = reader as IJsonLineInfo;
            return base.ReadJson(reader, objectType, existingValue, serializer);
        }

        protected override IJsonConvertable Create(Type objectType, JObject jObject)
        {

            JsonReader reader = jObject.CreateReader();
            try
            {
                reader.Read();
            }
            catch (Exception ex)
            {
                throw;
            }

            var jsonLineInfo = reader as IJsonLineInfo;

            return CreateInstance(objectType);

        }

        /// <summary>
        /// Создание экземпляра объекта с использованием лямбды (немного оптимальнее, чем Activator.CreateInstance)
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private IJsonConvertable CreateInstance(Type type)
        {

            var _typedFunc = Expression.Lambda<Func<IJsonConvertable>>(Expression.New(type)).Compile();

            return _typedFunc();
        }


        public ObjectJsonConverter()
        {

        }

    }
}

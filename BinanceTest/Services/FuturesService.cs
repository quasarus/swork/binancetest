﻿using BinanceTest.Converters;
using BinanceTest.Model.REST_Response;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BinanceTest.Services
{
    /// <summary>
    /// Сервис фьючерсов
    /// </summary>
    public class FuturesService : IFuturesService
    {
        public event Action<List<double[]>, List<double[]>> OnDepthUpdated;

        public Task<ExchangeRestResponse> GetExchangeAsync(CancellationTokenSource cts)
        {
            return Task.Run(() =>
            {
                //базовый URI
                Uri uri = new Uri("https://fapi.binance.com");
                //клиент REST
                IRestClient client = new RestClient(uri);
                //запрос к API фьючерсов
                IRestRequest request = new RestRequest("/fapi/v1/exchangeInfo", Method.GET);
                //выполнение запроса
                var task = client.ExecuteAsync(request, cts.Token);
                //проверка токена на отмену пользователем
                cts.Token.ThrowIfCancellationRequested();
                var response = task.Result;

                //проверка ответа
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    //настройки конвертера JSON
                    JsonSerializerSettings settings = new JsonSerializerSettings
                    {
                        Converters = new List<JsonConverter>() { new ObjectJsonConverter() },
                    };
                    //десериализация JSON с ответа REST с объект
                    var result = JsonConvert.DeserializeObject<ExchangeRestResponse>(response.Content, settings);
                    return result;
                }
                throw new Exception(response.ErrorException.Message);
            });
        }

        public Task SubscribeDepthUpdateAsync(string symbol, CancellationTokenSource cts)
        {

            return Task.Run(async () =>
            {
                try
                {
                    //инициализация вебсокета
                    using (ClientWebSocket ws = new ClientWebSocket())
                    {
                        //базовый URI
                        Uri serverUri = new Uri($"wss://fstream.binance.com/ws/{symbol.ToLower()}@depth5@500ms");
                        //подключение к вебсокету
                        await ws.ConnectAsync(serverUri, cts.Token);
                        //проверка токена на отмену пользователем
                        cts.Token.ThrowIfCancellationRequested();
                        //проверка состояния сокета
                        if (ws.State == WebSocketState.Open)
                        {
                            await SubscribeBookDepthStream(ws, symbol, cts);

                            try
                            {
                                //получение стакана в реальном времени
                                await ReceiveDepth(ws, cts, symbol);
                            }
                            catch (OperationCanceledException)
                            {
                                //при получении исключения по отмене пользователем - отписаться от стакана
                                await Unsubscribe(ws, symbol);
                                throw;
                            }


                        }

                    }
                }
                catch (Exception ex)
                {

                    throw;
                }

            });
        }

        /// <summary>
        /// Получение стакана
        /// </summary>
        /// <param name="ws"></param>
        /// <param name="cts"></param>
        /// <param name="symbol"></param>
        /// <returns></returns>
        private async Task ReceiveDepth(ClientWebSocket ws, CancellationTokenSource cts, string symbol)
        {
            StringBuilder sb = null;
            while (true)
            {
                //проверка токена на отмену пользователем
                cts.Token.ThrowIfCancellationRequested();

                //получение ответа
                ArraySegment<byte> bytesReceived = new ArraySegment<byte>(new byte[1024 * 100]);
                WebSocketReceiveResult result = await ws.ReceiveAsync(bytesReceived, CancellationToken.None);
                //проверка токена на отмену пользователем
                cts.Token.ThrowIfCancellationRequested();

                //преобразование в строку ответа
                string respString = Encoding.UTF8.GetString(bytesReceived.Array, 0, result.Count);

                Debug.Print(respString);

                /*
                 * Пояснение
                 * При подписывании одновременно на несколько стримов иногда происходит фрагментация
                 * полученных ответов, соответственно для нормальной десериализации полученных JSON, необходимо склеивать
                 * принятые строки, если они являются неполными с предыдущими
                 */



                //проверка, что полученная строка, является началом JSON ответа обновления стакана
                if (respString.StartsWith("{\"e\":\"depthUpdate\""))
                {
                    //добавить строку в стрингбилдер
                    sb = new StringBuilder();
                    sb.Append(respString);

                    //если полученная строка принята целиком, то десериализовать ее. ВНИМАНИЕ! Получение символа } -
                    //недостаточное условие того, что принята завершенная строка JSON, доработать //TODO
                    if (respString.EndsWith("}"))
                    {
                        //десериализация ответа
                        var res = JsonConvert.DeserializeObject<BidsAsksPayload>(sb.ToString());
                        //обнуление стрингбилдера - признак того, что ранее была получена завершенная для десериализации строка
                        sb = null;
                        //уведомление о получении стакана
                        OnDepthUpdated?.Invoke(res.BidsToBeUpdated, res.AsksToBeUpdated);
                        continue;
                    }
                }
                //если полученная строка не является началом JSON ответа обновления стакана
                else
                {
                    //если стрингбилдер ненулевой, значит ранее был получен незавершенная строка JSON
                    if (sb != null)
                    {
                        //защита от бесконечного заполнения стрингбилдера, считать, что в 10к символов должна быть хоть одна завершенная строка JSON
                        if (sb.Length < 10000)
                        {
                            sb = null;
                        }
                        else
                        {
                            //добавить полученную строку к стрингбилдеру
                            sb.Append(respString);

                            //если полученная строка принята целиком, то десериализовать ее. ВНИМАНИЕ! Получение символа } -
                            //недостаточное условие того, что принята завершенная строка JSON, доработать //TODO
                            if (respString.EndsWith("}"))
                            {
                                //десериализация ответа
                                var res = JsonConvert.DeserializeObject<BidsAsksPayload>(sb.ToString());
                                //обнуление стрингбилдера - признак того, что ранее была получена завершенная для десериализации строка
                                sb = null;
                                OnDepthUpdated?.Invoke(res.BidsToBeUpdated, res.AsksToBeUpdated);
                                continue;
                            }
                        }
                    }
                }

            }

        }

        /// <summary>
        /// Отписка от стакана
        /// </summary>
        /// <param name="ws"></param>
        /// <param name="symbol"></param>
        /// <returns></returns>
        private async Task Unsubscribe(ClientWebSocket ws, string symbol)
        {
            //проверка того, что сокет открыт. Формирование запроса и отправка
            if (ws.State == WebSocketState.Open)
            {
                string msgUNSUBSCRIBE = $"{{ \"method\":\"UNSUBSCRIBE\",\"params\":[\"{symbol.ToLower()}@depth\"],\"id\":1}}";
                ArraySegment<byte> bytesToSend = new ArraySegment<byte>(Encoding.UTF8.GetBytes(msgUNSUBSCRIBE));
                await ws.SendAsync(bytesToSend, WebSocketMessageType.Text, true, CancellationToken.None);
            }

        }


        /// <summary>
        /// Подписка на стакан
        /// </summary>
        /// <param name="ws">Сокет</param>
        /// <param name="symbol">Выбранный тикер</param>
        /// <param name="cts">Токен отмены</param>
        /// <returns></returns>
        private async Task SubscribeBookDepthStream(ClientWebSocket ws, string symbol, CancellationTokenSource cts)
        {
            //сообщение подписки согласно описанию API
            string msgSUBSCRIBE = $"{{\"method\":\"SUBSCRIBE\",\"params\":[\"{symbol.ToLower()}@depth\"],\"id\":1}}";
            //формирование запроса и отправка
            ArraySegment<byte> bytesToSend = new ArraySegment<byte>(Encoding.UTF8.GetBytes(msgSUBSCRIBE));
            await ws.SendAsync(bytesToSend, WebSocketMessageType.Text, true, cts.Token);
            cts.Token.ThrowIfCancellationRequested();
            ArraySegment<byte> bytesReceived = new ArraySegment<byte>(new byte[1024]);
            WebSocketReceiveResult result = await ws.ReceiveAsync(bytesReceived, cts.Token);

        }

    }
}

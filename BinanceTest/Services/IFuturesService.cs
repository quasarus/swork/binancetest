﻿using BinanceTest.Model.REST_Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BinanceTest.Services
{
    public interface IFuturesService
    {
        /// <summary>
        /// Получение фьючерсов
        /// </summary>
        /// <param name="cts">Токен отмены</param>
        /// <returns></returns>
        Task<ExchangeRestResponse> GetExchangeAsync(CancellationTokenSource cts);
        /// <summary>
        /// Подписка на обновление стакана выбранного тикера
        /// </summary>
        /// <param name="symbol">Выбранный тикер</param>
        /// <param name="cts">Токен отмены</param>
        /// <returns></returns>
        Task SubscribeDepthUpdateAsync(string symbol, CancellationTokenSource cts);

        /// <summary>
        /// Событие обновления стакана
        /// </summary>
        event Action<List<double[]>, List<double[]>> OnDepthUpdated;

    }
}

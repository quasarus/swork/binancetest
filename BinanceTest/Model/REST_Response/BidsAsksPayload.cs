﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinanceTest.Model.REST_Response
{
    /// <summary>
    /// Данные стакана
    /// </summary>
    public class BidsAsksPayload
    {
        [JsonProperty("e")]
        public string EventType {get; set;}
        [JsonProperty("E")]
        public string EventTime { get; set; }
        [JsonProperty("T")]
        public string TransactionTime { get; set; }
        [JsonProperty("s")]
        //Тикер
        public string Symbol { get; set; }
        [JsonProperty("b")]
        //Биды
        public List<double[]> BidsToBeUpdated { get; set; }
        [JsonProperty("a")]
        //Оферра
        public List<double[]> AsksToBeUpdated { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinanceTest.Model.REST_Response
{
    /// <summary>
    /// Конвертируемый в кастомный тип объект JSON
    /// </summary>
    public interface IJsonConvertable
    {

    }

    #region Типы данных для запрашиваемых фьючерсов
    public enum IntervalType
    {
        MINUTE,
        SECOND
    }
    public enum RateLimitType
    {
        ORDERS,
        REQUEST_WEIGHT
    }

    public enum ContractType
    {
        PERPETUAL,
        CURRENT_QUARTER,
        NEXT_MONTH,
        CURRENT_MONTH,
        NEXT_QUARTER
    }
    public enum SymbolStatusType
    {
        TRADING,
        PENDING_TRADING,
        PRE_DELIVERING,
        DELIVERING,
        DELIVERED,
        PRE_SETTLE,
        SETTLING,
        CLOSE
    }

    public enum UnderlyingType
    {
        COIN,
        INDEX
    }

    public enum OrderType
    {
        LIMIT,
        MARKET,
        STOP,
        STOP_MARKET,
        TAKE_PROFIT,
        TAKE_PROFIT_MARKET,
        TRAILING_STOP_MARKET
    }

    public enum TimeInForceType
    {
        GTC,
        IOC,
        FOK,
        GTX
    }
    #endregion

    /// <summary>
    /// Ответ на REST-запрос - фьючерсы
    /// </summary>
    public class ExchangeRestResponse
    {
        /// <summary>
        /// //TODO
        /// </summary>
        public List<string> exchangeFilters { get; set; }
        /// <summary>
        /// //TODO
        /// </summary>
        public List<RateLimit> rateLimits { get; set; }
        /// <summary>
        /// //TODO
        /// </summary>
        public long serverTime { get; set; }
        /// <summary>
        /// //TODO
        /// </summary>
        public List<ExchangeAsset> assets { get; set; }
        /// <summary>
        /// Тикеры
        /// </summary>
        public List<ExchangeSymbol> symbols { get; set; }
        public string timezone { get; set; }
    }

    /// <summary>
    /// //TODO
    /// </summary>
    public class RateLimit
    {
        public IntervalType interval { get; set; }
        public int intervalNum { get; set; }
        public int limit { get; set; }

        public RateLimitType rateLimitType { get; set; }
    }

    /// <summary>
    /// //TODO
    /// </summary>
    public class ExchangeAsset
    {
        public string asset { get; set; }
        public bool marginAvailable { get; set; }
        public double? autoAssetExchange { get; set; }
    }

    /// <summary>
    /// Фильтр для тикеров ??
    /// </summary>
    public class SymbolFilter : IJsonConvertable
    {
        public string filterType { get; set; }
    }

    #region Типы фильтров тикеров
    public class SymbolFilterPrice : SymbolFilter
    {
        public double maxPrice { get; set; }
        public double minPrice { get; set; }
        public double tickSize { get; set; }
    }

    public class SymbolFilterLotSize : SymbolFilter
    {
        public double maxQty { get; set; }
        public double minQty { get; set; }
        public double stepSize { get; set; }
    }

    public class SymbolFilterMarketLotSize : SymbolFilterLotSize
    {

    }

    public class SymbolFilterMaxNumOrders : SymbolFilter
    {
        public double limit { get; set; }
    }
    public class SymbolFilterMaxNumAlgoOrders : SymbolFilterMaxNumOrders
    {

    }

    public class SymbolFilterMaxNotional : SymbolFilter
    {
        public double multiplierUp { get; set; }
        public double multiplierDown { get; set; }
        public double multiplierDecimal { get; set; }
    }

    public class SymbolFilterPercentPrice : SymbolFilter
    {
        public double notional { get; set; }
    }
    #endregion

    /// <summary>
    /// Данные тикера
    /// </summary>
    public class ExchangeSymbol
    {
        public string symbol { get; set; }
        public string pair { get; set; }

        public ContractType? contractType { get; set; }

        public long deliveryDate { get; set; }

        public long onboardDate { get; set; }

        public SymbolStatusType status { get; set; }

        public double maintMarginPercent { get; set; }

        public double requiredMarginPercent { get; set; }

        public string baseAsset { get; set; }

        public string quoteAsset { get; set; }

        public string marginAsset { get; set; }

        public double pricePrecision { get; set; }

        public double quantityPrecision { get; set; }

        public double baseAssetPrecision { get; set; }

        public double quotePrecision { get; set; }
        public UnderlyingType underlyingType { get; set; }
        public List<string> underlyingSubType { get; set; }
        public int settlePlan { get; set; }
        public double triggerProtect { get; set; }
        public List<SymbolFilter> filters { get; set; }

        public List<OrderType> OrderType { get; set; }
        public List<string> timeInForce { get; set; }

        public double liquidationFee { get; set; }
        public double marketTakeBound { get; set; }
    }
}

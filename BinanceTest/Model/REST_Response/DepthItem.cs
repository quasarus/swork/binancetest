﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinanceTest.Model.REST_Response
{
    /// <summary>
    /// Запись стакана
    /// </summary>
    public class DepthItem : BindableBase
    {
        /// <summary>
        /// Количество
        /// </summary>
        private string _quantity;
        public string Quantity
        {
            get { return _quantity; }
            set { SetProperty(ref _quantity, value); }
        }

        /// <summary>
        /// Цена
        /// </summary>
        private string _price;
        public string Price
        {
            get { return _price; }
            set { SetProperty(ref _price, value); }
        }
    }

    /// <summary>
    /// Запись стакана - оффер
    /// </summary>
    public class DepthItemOffer: DepthItem
    {

    }

    /// <summary>
    /// Запись стакана - бид
    /// </summary>
    public class DepthItemBid : DepthItem
    {

    }
}

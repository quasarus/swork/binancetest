﻿using BinanceTest.Model.REST_Response;
using BinanceTest.Services;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BinanceTest.ViewModels
{
    /// <summary>
    /// Стартовая Вьюмодель 
    /// </summary>
    public class StartViewModel : BindableBase, INavigationAware
    {
        //Основной токен отмены пользователем
        private CancellationTokenSource Cts;
        //задача подписки на стакан
        private Task subTask;
        //служба фьючерсов
        private IFuturesService _futuresService;

        //Команда обновления фьючерсов с кнопки
        public DelegateCommand UpdateFuturesCommand { get; set; }

        /// <summary>
        /// Фьючерсы
        /// </summary>
        private ObservableCollection<string> _futures;
        public ObservableCollection<string> Futures
        {
            get { return _futures; }
            set { SetProperty(ref _futures, value); }
        }

        /// <summary>
        /// Стакан
        /// </summary>
        private ObservableCollection<DepthItem> _depth;
        public ObservableCollection<DepthItem> Depth
        {
            get { return _depth; }
            set { SetProperty(ref _depth, value); }
        }

        /// <summary>
        /// Выполняется ли запрос фьючерсов
        /// </summary>
        private bool _isExchangeRequest;
        public bool IsExchangeRequest
        {
            get { return _isExchangeRequest; }
            set { SetProperty(ref _isExchangeRequest, value); }
        }

        /// <summary>
        /// Фыбранный фьючерс
        /// </summary>
        private string _selectedFutures;
        public string SelectedFutures
        {
            get { return _selectedFutures; }
            set
            { 
                SetProperty(ref _selectedFutures, value);
            }
        }

        public StartViewModel(IFuturesService futuresService)
        {
            _futuresService = futuresService;

            UpdateFuturesCommand = new DelegateCommand(async () =>
            {
                //при команде обновления фьючерсов, выполнить, если команда уже не выполняется
                using (Cts = new CancellationTokenSource())
                {
                    if (!IsExchangeRequest)
                    {
                        await UpdateFutures();
                    }
                    if (Cts.IsCancellationRequested)
                    {
                        return;
                    }
                }
                Cts = null;
            });
        }

        //является ли представление этой вьюмодели целью навигации (для ведения журнала навигации региона Prism)
        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        /// <summary>
        /// При навигации от представления этой вьюмодели
        /// </summary>
        /// <param name="navigationContext"></param>
        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            //обнуление стакана
            Depth = null;
            //отписка от события изменения свойства этой вьюмодели
            this.PropertyChanged -= StartViewModel_PropertyChanged;
        }

        /// <summary>
        /// При навигации к представлению этой вьюмодели
        /// </summary>
        /// <param name="navigationContext"></param>
        public async void OnNavigatedTo(NavigationContext navigationContext)
        {
            //инициализация стакана
            Depth = new ObservableCollection<DepthItem>();
            //заполнить стакан элементами офферов
            for (int i = 0; i < 5; i++)
            {
                Depth.Add(new DepthItemOffer());
            }

            //заполнить стакан элементами бидов
            for (int i = 0; i < 5; i++)
            {
                Depth.Add(new DepthItemBid());
            }

            //выполнить обновление фьючерсов, если команда уже не выполняется
            using (Cts = new CancellationTokenSource())
            {
                if (!IsExchangeRequest)
                {
                    await UpdateFutures();
                }
                if (Cts.IsCancellationRequested)
                {
                    return;
                }
            }
            Cts = null;
            this.PropertyChanged += StartViewModel_PropertyChanged;

        }


        /// <summary>
        /// Событие обновления св-ва этой вьюмодели
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void StartViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            //если обновляемое св-во - выбранный фьючерс и тикер выбранного фьючерса не пустой
            if (e.PropertyName == nameof(SelectedFutures) && !string.IsNullOrEmpty(SelectedFutures))
            {
                //отмена токен, если он ненулевой
                Cts?.Cancel();
                

                try
                {
                    //если уже выполняется задача обновления стакана
                    if (subTask != null && !subTask.IsCompleted)
                    {
                        //ожидание ее корректного завершения токеном отмены, она сгенерирует
                        //исключение отмены операции, либо еще какое-либо исключение, игнорировать его
                        try
                        {
                            await subTask;
                        }
                        catch (Exception)
                        {
                            //TODO
                        }
                        
                    }
                    //создание нового токена отмены
                    Cts = new CancellationTokenSource();
                    //подписка на событие обновления стакана
                    _futuresService.OnDepthUpdated += _OnDepthUpdated;
                    //подписка на обновление стакана
                    subTask = _futuresService.SubscribeDepthUpdateAsync(SelectedFutures, Cts);
                    //ожидание завершения задачи обновления стакана
                    await subTask;

                }
                catch (OperationCanceledException) //исключение по отмене
                {
                   
                    return;
                }
                catch (AggregateException aex) //при генерации группы исключений
                {
                    if (aex.Flatten().InnerExceptions.Any(f => f is OperationCanceledException))
                    {
                        return;
                    }
                }
                catch (Exception ex)            //при остальных исключениях
                {
                    //TODO
                } 
                finally                         //в любом случае отписка от события обновления стакана
                {
                    _futuresService.OnDepthUpdated -= _OnDepthUpdated;
                }
            }
        }

        /// <summary>
        /// При закрытии приложения
        /// </summary>
        /// <returns></returns>
        public async Task OnAppClosing()
        {
            //оменить основной токен
            Cts?.Cancel();
            try
            {
                //дождаться корректного завершения задачи обновления стакана
                if (subTask != null && !subTask.IsCompleted)
                {
                    await subTask;
                }                
            }
            catch (Exception)
            {

                return;
            }
        }

        /// <summary>
        /// По событию обновления стакана
        /// </summary>
        /// <param name="bids">Данные бидов</param>
        /// <param name="offers">Данные офферов</param>
        private void _OnDepthUpdated(List<double[]> bids, List<double[]> offers)
        {
            //если число бидов и офферов равно запрашиваемому числу
            if (bids.Count == 5 && offers.Count == 5)
            {
                //перевернуть оффера от высокой цены к низкой
                offers.Reverse();

                //заполнить стакан данными
                for (int cnt = 0; cnt < Depth.Count / 2; cnt++)
                {
                    Depth[cnt].Price = offers[cnt][0].ToString();
                    Depth[cnt].Quantity = offers[cnt][1].ToString();

                    Depth[cnt + 5].Price = bids[cnt][0].ToString();
                    Depth[cnt + 5].Quantity = bids[cnt][1].ToString();
                }
            }
        }

        /// <summary>
        /// Обновление списка фьючерсов
        /// </summary>
        /// <returns></returns>
        private async Task UpdateFutures()
        {
            SelectedFutures = null;
            try
            {
                //установка флага выполнения запроса обновления фьючерсов
                IsExchangeRequest = true;
                //получение фьючерсов
                var exchange = await _futuresService.GetExchangeAsync(Cts);
                Cts.Token.ThrowIfCancellationRequested();
                //выбор фьючерсов только типа COIN
                var futures = exchange.symbols.Where(f => f.underlyingType == UnderlyingType.COIN);
                //заполнение тикеров фьючерсов
                Futures = new ObservableCollection<string>(futures.Select(g => g.symbol));
            }
            catch (OperationCanceledException)
            {
                return;
            }
            catch (AggregateException aex)
            {
                if (aex.Flatten().InnerExceptions.Any(f=>f is OperationCanceledException))
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                //TODO
            }
            finally
            {
                //в лючом случае - снятие флага выполнения запроса обновления фьючерсов
                IsExchangeRequest = false;
            }
        }
    }
}

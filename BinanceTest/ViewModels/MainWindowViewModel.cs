﻿using BinanceTest.Model.REST_Response;
using BinanceTest.Services;
using BinanceTest.Views;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace BinanceTest.ViewModels
{

    /// <summary>
    /// Вьюмодель главной страницы
    /// </summary>
    public class MainWindowViewModel : BindableBase, INavigationAware
    {
        /// <summary>
        /// Команда выполняется при окончании загрузки главного окна
        /// </summary>
        public DelegateCommand MainWindowLoadedCommand { get; set; }

        private readonly IRegionManager _regionManager;
        private readonly IEventAggregator _eventAggregator;



        private string _title = "BinanceTest";
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }




        public MainWindowViewModel(IRegionManager regionManager, IEventAggregator eventAggregator)
        {
            _regionManager = regionManager;
            _eventAggregator = eventAggregator;
            //при окончании загрузки главного окна перейти на стартовый вид
            MainWindowLoadedCommand = new DelegateCommand(()=>
            {
                _regionManager?.RequestNavigate("BaseRegion",
new Uri(nameof(StartView), UriKind.Relative));
            });

        }



        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            
        }
    }
}

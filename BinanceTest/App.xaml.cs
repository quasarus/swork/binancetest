﻿using BinanceTest.Services;
using BinanceTest.ViewModels;
using BinanceTest.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;
using System.Linq;
using System.Windows;
using System.Windows.Navigation;

namespace BinanceTest
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        MainWindow mainWindow;
        /// <summary>
        /// Создание основного окна
        /// </summary>
        /// <returns></returns>
        protected override Window CreateShell()
        {
            mainWindow = Container.Resolve<MainWindow>();
            //подписка на событие акрытия основного окна
            mainWindow.Closing += App_Closing;
            return mainWindow;
        }

        /// <summary>
        /// Регистрация типов контейнера DI
        /// </summary>
        /// <param name="containerRegistry"></param>
        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            //регистрация службы фьючерсов
            containerRegistry.Register<IFuturesService, FuturesService>();
            //регистрация стартового представления и его VM с возможностью навигации
            containerRegistry.RegisterForNavigation<StartView, StartViewModel>();
            
        }

        /// <summary>
        /// По закрытию главного окна
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void App_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //отписка от события
            mainWindow.Closing -= App_Closing;
            //отмена закрытия окна
            e.Cancel = true;

            //получение расширения контейнера DI
            var cex = Container.Resolve<IContainerExtension>();
            //получение менеджера регионов Prism
            var regm = cex.Resolve<IRegionManager>();
            //получение ViewModel стартового вида из DataContext стартового вида
            var startView = regm.Regions["BaseRegion"].Views.OfType<StartView>().SingleOrDefault().DataContext;
            //если ViewModel стартового вида существует, то вызвать метод по закрытию приложения
            if (startView != null)
            {
                await (startView as StartViewModel)?.OnAppClosing();
            }
            //подтверждение закрытия окна
            e.Cancel = false;
            //закрытие приложения
            App.Current.Shutdown(0);
        }
    }
}
